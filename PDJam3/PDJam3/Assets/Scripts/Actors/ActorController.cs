﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActorController : MonoBehaviour 
{
	[SerializeField]
	private Animator anim;

	private const float MOVE_SPEED = 0.2f;

	public void MoveToPosition(Vector2 position)
	{
		GetComponent<LinearLerpPosition> ().LerpTo (position, 0.2f);

		bool flipped = position.x < transform.position.x;
		transform.localScale = new Vector2 (Mathf.Abs (transform.localScale.x) * (flipped ? -1f : 1f), transform.localScale.y);
	}

	void Update()
	{
		anim.SetBool ("walking", GetComponent<LinearLerpPosition> ().Moving);
		if (GetComponent<LinearLerpPosition> ().Moving)
		{
			anim.speed = 1.5f;
		} else
		{
			anim.speed = 1f;
		}
	}

	#if UNITY_EDITOR
	private bool movedLeft;
	void LateUpdate()
	{
		if (Input.GetKeyDown (KeyCode.M))
		{
			movedLeft = !movedLeft;
			MoveToPosition (new Vector2 (movedLeft ? 2f : -2f, transform.position.y));
		}
	}
	#endif
}

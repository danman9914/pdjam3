﻿using UnityEngine;

public class ScoreKeeper: MonoBehaviour
{
    public CrowdBar CrowdBar;
    public float MaxScore = 10f;

    private float _score;

    void Start()
    {
         _score = MaxScore / 2;
    }

    public void UpdateScore(float points)
    {
        _score += points;
        var scoreBarPercentage = _score / MaxScore;

        if (scoreBarPercentage > 1.025f)
        {
            scoreBarPercentage = 1.025f;
            _score = MaxScore;
        }
        else if (scoreBarPercentage < 0f)
        {
            scoreBarPercentage = 0f;
            _score = 0f;
        }

        CrowdBar.SetBarPercent(scoreBarPercentage);
    }

    public float GetScore()
    {
        return _score;
    }
}

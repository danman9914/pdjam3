﻿[System.Serializable]
public class AnswerOption
{
    public string Text;
    public bool IsSelected;
    public int Multiplier;
	public LineAction Action;
}

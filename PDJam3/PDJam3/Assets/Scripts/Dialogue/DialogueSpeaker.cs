﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogueSpeaker : MonoBehaviour
{
	[HideInInspector]
	public bool isSpeaking;

	[SerializeField]
	private CharacterMouth mouth;

	[SerializeField]
	private string voiceSoundId;

	[SerializeField]
	private float voicePitchShift;

	[SerializeField]
	private float voiceVolumeShift;

	protected DialogueBubble currentBubble;

	private const string BUBBLE_PREFAB_PATH = "Prefabs/DialogueBubble";
	public virtual void EndDialogue() {}

#if UNITY_EDITOR
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.D))
		{
			SpeakMessage ("Et tu, Brute?");
		}
	}
#endif

	public void SpeakMessage(string message)
	{
		isSpeaking = true;
		currentBubble = InstantiateBubble ();
		currentBubble.ShowMessage (message);
		GetComponentInChildren<Animator> ().SetBool ("speaking", true);
	}

	protected DialogueBubble InstantiateBubble()
	{
		GameObject bubbleObj = Instantiate (Resources.Load (BUBBLE_PREFAB_PATH) as GameObject);
		DialogueBubble bubble = bubbleObj.GetComponent<DialogueBubble> ();

		bubble.text.DisplayCharacter += OnCharacterDisplayed;
		bubble.text.EndMessage += OnMessageEnded;

		bubble.PositionBubble (transform);

		return bubble;
	}

	private void OnCharacterDisplayed()
	{
		float voicePitch = (float)(UnityEngine.Random.Range (95, 105)) * 0.01f;
		AudioController.Instance.PlaySoundAtLimitedFrequency (voiceSoundId, 0.05f, voicePitch + voicePitchShift, 1f + voiceVolumeShift);
		mouth.ToggleMouthOpen ();
	}

	private void OnMessageEnded()
	{
		GetComponentInChildren<Animator> ().SetBool ("speaking", false);
		mouth.CloseMouth ();
		currentBubble.Destroy ();
		currentBubble.text.DisplayCharacter -= OnCharacterDisplayed;
		currentBubble.text.EndMessage -= OnMessageEnded;
	}
}

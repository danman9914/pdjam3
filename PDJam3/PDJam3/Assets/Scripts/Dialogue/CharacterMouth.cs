﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
public class CharacterMouth : MonoBehaviour 
{
	public Sprite openSprite;
	public Sprite closedSprite;

	private bool mouthIsOpen;

	public void ToggleMouthOpen()
	{
		if (mouthIsOpen)
		{
			CloseMouth ();
		}
		else 
		{
			OpenMouth ();
		}
	}

	public void OpenMouth()
	{
		GetComponent<SpriteRenderer> ().sprite = openSprite;
		mouthIsOpen = true;
	}

	public void CloseMouth()
	{
		GetComponent<SpriteRenderer> ().sprite = closedSprite;
		mouthIsOpen = false;
	}
}
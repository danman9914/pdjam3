﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogueBubble : MonoBehaviour 
{
	public SequentialText text;

	[SerializeField]
	private Transform pointer;

	private Vector2 originalPosition;

	private readonly Vector2 HEAD_OFFSET = new Vector2(1f, 2.5f);

	void Start()
	{
		transform.localScale = new Vector2(0f, 0f);
		GetComponent<LerpScale> ().Scale = Vector2.one;
	}

	public void PositionBubble(Transform character)
	{
		if (character.transform.position.x < 0f)
		{
			transform.position = (Vector2)character.transform.position + HEAD_OFFSET;
		}
		else 
		{
			transform.position = (Vector2)character.transform.position + new Vector2(-HEAD_OFFSET.x, HEAD_OFFSET.y);
			transform.GetChild (0).localPosition = new Vector2 (-transform.GetChild (0).localPosition.x, transform.GetChild (0).localPosition.y);
			pointer.localPosition = new Vector2 (-pointer.localPosition.x, pointer.localPosition.y);
			pointer.localScale = new Vector2 (-1f, 1f);
		}

		originalPosition = transform.localPosition;
		GetComponent<LerpPosition> ().LocalPosition = originalPosition;
	}

	public void ShowMessage(string message)
	{
		StartCoroutine (ShowMessageDelayed (message));
	}

	public void Destroy()
	{
		StopAllCoroutines ();
		StartCoroutine (DestroyBubble ());
	}

	private IEnumerator ShowMessageDelayed(string message)
	{
		yield return new WaitForSeconds (0.1f);
		text.EndMessage += OnMessageEnded;
		text.PlayMessage (message, "default_voice");
	}

	private IEnumerator DestroyBubble()
	{
		GetComponent<LerpScale> ().Scale = new Vector2 (0f, 0f);
		yield return new WaitForSeconds (1f);
		Destroy (gameObject);
		yield break;
	}

#region Text Events

	private void OnMessageEnded()
	{
		text.EndMessage -= OnMessageEnded;
	}

#endregion
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(TMPro.TextMeshPro))]
public class SequentialText : MonoBehaviour 
{
	public event System.Action DisplayCharacter;
	public event System.Action EndMessage;

	private const float DEFAULT_FREQUENCY = 0.075f;

	[HideInInspector]
	public float characterFrequency = DEFAULT_FREQUENCY;

	[HideInInspector]
	public bool typing = false;

	private TMPro.TextMeshPro textMesh;
	private string message;

	void Awake()
	{
		textMesh = GetComponent<TMPro.TextMeshPro> ();
	}

	public void PlayMessage(string message, string SFXName) {
		this.message = message;
		if (textMesh.gameObject.activeInHierarchy) {
			StartCoroutine (Type (SFXName));
		}
	}

	public void Clear() {
		StopAllCoroutines();
		typing = false;

		textMesh.text = "";
		message = "";
	}

	private IEnumerator Type(string SFXName) {
		typing = true;
		textMesh.text = "";

		int index = 0;
		while (textMesh.text.Length < message.Length) {
			if (message[index] != ' ') {
				if (DisplayCharacter != null)
				{
					DisplayCharacter ();
				}
			}
			textMesh.text += message[index];
			index++;

			yield return new WaitForSeconds (characterFrequency);
		}

		yield return new WaitForSeconds(0.5f);

		typing = false;

		if (EndMessage != null)
		{
			EndMessage ();
		}
	}
}
﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CrowdBar : MonoBehaviour 
{
    [SerializeField]
    private string _successNoiseName;

	[SerializeField]
	private Animator cheeringAnim;

	[SerializeField]
	private LerpScale barFill;

    [SerializeField]
    private Color32 GreenBar = new Color32(20, 128, 9, 255);
    [SerializeField]
    private Color32 OrangeBar = new Color32(176, 108, 20, 255);
    [SerializeField]
    private Color32 RedBar = new Color32(149, 46, 46, 255);

    [SerializeField]
    private float UpperRange = 0.7f;
    [SerializeField]
    private float LowerRange = 0.4f;

    private float intendedFillSize;

	private const float FILLED_BAR_AMOUNT = 38f;

    private SpriteRenderer _fillRenderer;

	void Start()
	{
        _fillRenderer = transform.FindChild("fill").gameObject.GetComponent<SpriteRenderer>();

		intendedFillSize = barFill.transform.localScale.x - 0.1f;
		SetBarPercent (0.5f, true);
	}

	public void SetBarPercent(float normalizedAmount, bool isStart = false)
	{
		intendedFillSize = FILLED_BAR_AMOUNT * normalizedAmount;
		barFill.Scale = new Vector2 (intendedFillSize, barFill.Scale.y);

		//duplicate from below
		float barSize = barFill.transform.localScale.x;
		bool barMovingUp = intendedFillSize > barSize;
		//

		if (barMovingUp && !isStart)
		{
			CrowdAnimation.instance.Cheer ();
            
            AudioController.Instance.PlayRandomSound(_successNoiseName, true);
        }
        else if (!isStart)
        {
            AudioController.Instance.PlayRandomSound("boo", true);
        }
	}

	void Update()
	{
        float barSize = barFill.transform.localScale.x;
		bool barIsMoving = Mathf.Abs (intendedFillSize - barSize) > 0.5f;
		bool barMovingUp = intendedFillSize > barSize;

		cheeringAnim.SetBool ("cheering", barIsMoving && barMovingUp);
		cheeringAnim.SetBool ("jeering", barIsMoving && !barMovingUp);

        if (barIsMoving)
            SetBarColor(barSize);

#if UNITY_EDITOR
        if (Input.GetKeyDown (KeyCode.B))
		{
			SetBarPercent (Random.value);
		}
#endif
	}

    private void SetBarColor(float barSize)
    {
        if (barSize > FILLED_BAR_AMOUNT * UpperRange)
        {
            _fillRenderer.color = GreenBar;
        }
        else if (barSize >= FILLED_BAR_AMOUNT * LowerRange && barSize <= FILLED_BAR_AMOUNT * UpperRange)
        {
            _fillRenderer.color = OrangeBar;
        }
        else
        {
            _fillRenderer.color = RedBar;
        }
    }
}

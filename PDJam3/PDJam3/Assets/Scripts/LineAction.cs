﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class LineAction
{
	public float actionWaitTime = 2f;

	[Header ("Enter Actor")]
	public GameObject enterActor;

	[Header("Dialogue")]
	public DialogueSpeaker speaker;
	public string dialogueLine;

	[Header("Animation")]
	public Animator animator;
	public string animationTrigger;
	public string animationBool;

	[Header("Movement")]
	public ActorController actor;
	public Transform newStagePosition;

	[Header("Got Item")]
	public Animator animatorForItem;
	public SpriteRenderer itemOnCharacter;
	public Sprite item;

	public void Activate(string completedLineText = "")
	{
		if (enterActor != null)
		{
			enterActor.SetActive (true);
		}
		if (speaker != null)
		{
			speaker.SpeakMessage (string.IsNullOrEmpty(dialogueLine) ? completedLineText : dialogueLine);
		}

		if (animator != null)
		{
			if (!string.IsNullOrEmpty (animationTrigger))
			{
				animator.SetTrigger (animationTrigger);
			}
			if (!string.IsNullOrEmpty (animationBool))
			{
				animator.SetBool (animationBool, true);
			}
		}

		if (actor != null && newStagePosition != null)
		{
			actor.MoveToPosition (newStagePosition.position);
		}

		if (itemOnCharacter != null)
		{
			itemOnCharacter.sprite = item;
			if (animatorForItem != null)
			{
				animatorForItem.SetTrigger ("drawitem");
			}
		}
	}
}

﻿using UnityEngine;
using TMPro;

public class ClickAndDragOption : MonoBehaviour {

    public LineDisplayerManager DisplayManager;

    [HideInInspector]
    public bool IsDraggable = true;

    private bool _isInsideLine;
    private Vector3 _positionOffset;
    private float _xPosition;
    private float _yPosition;

    void OnMouseUp()
    {
        if (IsDraggable)
        {
            DecreaseSortOrder();
            AudioController.Instance.PlaySound("paper_1");
            //if it is inside the box after release of the mouse it should trigger the event to LineDisplayerManager
            if (_isInsideLine)
            {
                transform.gameObject.SetActive(false);
                DisplayManager.OptionSelected(transform.FindChild("text").GetComponent<TextMeshPro>().text);
            }
            else
            {
                //move back to the position it started
                transform.position = new Vector2(_xPosition, _yPosition);
            }
        }
    }

    void OnMouseDown()
    {
        if (IsDraggable)
        {
            IncreaseSortOrder();
            AudioController.Instance.PlaySound("paper_1");
            _xPosition = transform.position.x;
            _yPosition = transform.position.y;
            _positionOffset = transform.position - Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
        }
    }

    void OnMouseDrag()
	{
        if (IsDraggable)
        {
            var curScreenPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            var curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + _positionOffset;
            transform.position = curPosition;
        }
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.name == "LineText")
        {
            DisplayManager.OptionIsInsideLine();
            _isInsideLine = true;
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.name == "LineText")
        {
            DisplayManager.OptionIsOutsideLine();
            _isInsideLine = false;
        }
    }

    private void IncreaseSortOrder()
    {
        gameObject.GetComponentInChildren<TextMeshPro>().sortingOrder += 5;
        gameObject.GetComponentInChildren<SpriteRenderer>().sortingOrder += 5;
    }

    private void DecreaseSortOrder()
    {
        gameObject.GetComponentInChildren<TextMeshPro>().sortingOrder -= 5;
        gameObject.GetComponentInChildren<SpriteRenderer>().sortingOrder -= 5;
    }
}

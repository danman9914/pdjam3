﻿using UnityEngine;
using System.Linq;
using TMPro;
using System.Collections.Generic;

public class LineDisplayerManager : MonoBehaviour {

    public GameObject LineObject;
    public List<GameObject> WordOptionObjects;

    private Line _line;
    private bool _isChoosing;

	public void SetSheetDetails(Line line, Line lastReferenceLine)
    {
        _line = line;
        _isChoosing = true;

		line.KnownText = FillInReferences(line, lastReferenceLine);
        LineObject.GetComponent<TextMeshPro>().text = line.KnownText;

        if (line.AnswerOptions.Count > 0)
            LineObject.GetComponent<Collider2D>().enabled = true;

        var index = 0;
        foreach (var word in line.AnswerOptions)
        {
            var go = WordOptionObjects[index];
            go.transform.FindChild("text").GetComponent<TextMeshPro>().text = word.Text;
			SetOptionSortOrder (go);
            go.SetActive(true);
            index++;
        }
    }

	private string FillInReferences(Line line, Line referenceLine)
	{
		string newText = line.KnownText;
		if (line.KnownText.Contains ("[X]") && referenceLine != null)
		{
			newText = newText.Replace ("[X]", referenceLine.ChosenOption.Text);
		}
		return newText;
	}

	private void SetOptionSortOrder(GameObject option)
	{
		int parentOrder = transform.parent.GetComponent<SpriteRenderer> ().sortingOrder;
		option.GetComponentInChildren<TextMeshPro> ().sortingOrder = parentOrder + 3;
		option.GetComponentInChildren<SpriteRenderer> ().sortingOrder = parentOrder + 2;
	}

    public void OptionSelected(string chosenWord)
    {
        LineObject.GetComponent<BoxCollider2D>().enabled = false;

        foreach (var wordOption in WordOptionObjects)
            wordOption.GetComponent<ClickAndDragOption>().IsDraggable = false;

        _isChoosing = false;
        _line.AnswerOptions.Single(x => x.Text == chosenWord).IsSelected = true;
		_line.KnownText = FormatLineWithChosenWord(_line.KnownText, chosenWord);
		LineObject.GetComponent<TextMeshPro> ().text = _line.KnownText;
        LineObject.GetComponent<TextMeshPro>().color = new Color32(0, 0, 0, 255);
    }

    public void OptionIsInsideLine()
    {
        LineObject.GetComponent<TextMeshPro>().color = new Color32(149, 46, 46, 255);
    }

    public void OptionIsOutsideLine()
    {
        LineObject.GetComponent<TextMeshPro>().color = new Color32(0, 0, 0, 255);
    }

    public Line GetLine()
    {
        return _line;
    }


    public bool GetChoiceState()
    {
        return _isChoosing;
    }

    private string FormatLineWithChosenWord(string line, string chosenWord)
    {
        int firstDash = line.IndexOf("-");
        int lastDash = line.LastIndexOf("-");
        return string.Format("{0}{1}{2}", line.Substring(0, firstDash), chosenWord, line.Substring(lastDash + 1));
    }

}

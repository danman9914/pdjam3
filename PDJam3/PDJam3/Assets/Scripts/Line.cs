﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Line {

    public string KnownText;
    public List<AnswerOption> AnswerOptions;
    public LineAction NoOptionsAction;
	public bool IsReferenceLine;

	[HideInInspector]
	public AnswerOption ChosenOption;
}

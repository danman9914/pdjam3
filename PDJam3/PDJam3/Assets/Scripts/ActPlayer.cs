﻿using UnityEngine;
using System.Linq;
using System.Collections;

public class ActPlayer : MonoBehaviour
{
    public PageFlipper PageFlipper;
    public Act Act;
    public ScoreKeeper ScoreKeeper;

    private LineDisplayerManager _lineDisplayManager;
	private Line _lastReferenceLine;

	public int debugSkipStartIndex;
	public int debugSkipEndIndex;

    void Start()
    {
        StartCoroutine(FillInLines(Act));
    }

    private IEnumerator FillInLines(Act act)
    {
		yield return new WaitForSeconds (5f);

#if UNITY_EDITOR
		Act.LineList.RemoveRange(debugSkipStartIndex, debugSkipEndIndex - debugSkipStartIndex);
#endif

        foreach (var line in Act.LineList)
        {
            GameObject newPage = PageFlipper.FlipPage(line.KnownText);

			_lineDisplayManager = newPage.GetComponentInChildren<LineDisplayerManager>();
			_lineDisplayManager.SetSheetDetails (line, _lastReferenceLine);

			if (line.AnswerOptions.Count > 0)
			{
				if (Tutorial.instance)
				{
					Tutorial.instance.GetComponent<Renderer> ().enabled = true;
				}
                PageFlipper.Raise();
                
				while (_lineDisplayManager.GetChoiceState ())
				{
					yield return new WaitForSeconds (1);
                }

                PageFlipper.Lower();

                var newLine = _lineDisplayManager.GetLine ();
				var chosenOption = newLine.AnswerOptions.SingleOrDefault (x => x.IsSelected.Equals (true));

				line.ChosenOption = chosenOption;
				chosenOption.Action.Activate (line.KnownText);
				if (line.IsReferenceLine)
				{
					_lastReferenceLine = line;
				}
				yield return new WaitForSeconds (chosenOption.Action.actionWaitTime);

                ScoreKeeper.UpdateScore(chosenOption.Multiplier);

                yield return new WaitForSeconds(0.5f);

				if (Tutorial.instance)
				{
					Destroy (Tutorial.instance.gameObject);
				}
            } 
			else
			{
				line.NoOptionsAction.Activate(line.KnownText);
				yield return new WaitForSeconds (line.NoOptionsAction.actionWaitTime);
			}
        }
		AudioController.Instance.PlaySound ("Applause");
		ActIntroController.instance.EndScene ();
    }
}

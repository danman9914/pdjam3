﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class PageFlipper : MonoBehaviour 
{
	[SerializeField]
	private GameObject pagePrefab;

	[SerializeField]
	private List<LerpPosition> pages;

	[SerializeField]
	private Animation anim;

	[SerializeField]
	private string initialLineText;

	private Vector2 page3StartPosition;
	private Vector2 page2StartPosition;
	private Vector2 page1StartPosition;

	private float originalYPos;
	private const float RAISE_AMOUNT = 2f;

	private const float OFFSCREEN_AMOUNT = -4.5f;

	#if UNITY_EDITOR
	private List<string> testMessages = new List<string>()
	{
		"Hello",
		"Stephen",
		"Brendan",
		"and Josh.",
		"Et tu, Brute?",
	};
	private int testIndex;

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Space))
		{
			testIndex = (testIndex + 1) % testMessages.Count;
			FlipPage (testMessages [testIndex]);
		}

		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			Raise ();
		}

		if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			Lower ();
		}
	}
	#endif

	void Start()
	{
		page3StartPosition = pages [2].LocalPosition;
		page2StartPosition = pages [1].LocalPosition;
		page1StartPosition = pages [0].LocalPosition;

		originalYPos = transform.position.y;

		for (int i = 0; i < pages.Count; i++)
		{
			pages [i].GetComponentInChildren<TextMeshPro> ().sortingOrder = 1 - (i * 2);
		}

		//Flip out the dummy pages so that the first page is an instance of the LineSheet prefab
		FlipPage ("");
		FlipPage ("");
		FlipPage (initialLineText);
	}

	public void Raise()
	{
		GetComponent<LerpPosition> ().LocalPosition = new Vector2 (transform.position.x, originalYPos + RAISE_AMOUNT);
	}

	public void Lower()
	{
		GetComponent<LerpPosition> ().LocalPosition = new Vector2 (transform.position.x, originalYPos);
	}

	public GameObject FlipPage(string newLineText)
	{
		pages [2].LocalPosition = page2StartPosition;
		pages [1].LocalPosition = page1StartPosition;
		pages [0].LocalPosition = page1StartPosition - Vector2.down * OFFSCREEN_AMOUNT + Vector2.left * 0.5f;

		anim.Play ();
		pages [1].GetComponentInChildren<TextMeshPro> ().SetText (newLineText);

		GameObject newPage = AddPage ();
		Destroy (pages [0].gameObject, 1f);
		pages.RemoveAt (0);

		return pages[0].gameObject;
	}

	private GameObject AddPage()
	{
		GameObject newPage = Instantiate (pagePrefab);
		newPage.transform.parent = anim.transform;
		newPage.transform.localPosition = page3StartPosition - Vector2.down * OFFSCREEN_AMOUNT;
		newPage.transform.localRotation = Quaternion.identity;

		newPage.GetComponent<SpriteRenderer> ().sortingOrder = pages [2].GetComponent<SpriteRenderer> ().sortingOrder - 5;
		newPage.GetComponentInChildren<TextMeshPro> ().sortingOrder = pages [2].GetComponentInChildren<TextMeshPro> ().sortingOrder - 5;

		LerpPosition pagePos = newPage.GetComponent<LerpPosition> ();
		pagePos.LocalPosition = page3StartPosition;

		pages.Add (pagePos);

		return newPage;
	}
}

﻿using UnityEngine;
using System.Collections;

public class LerpPosition : MonoBehaviour {

	public float speed = 2.5f;

    public Vector2 Position
    {
        get
        {
            return transform.position;
        }

        set
        {
            intendedPos = value;
            local = false;
        }
    }
    public Vector2 LocalPosition
    {
        get
        {
            return transform.position;
        }

        set
        {
            intendedPos = value;
            local = true;
        }
    }

    private Vector2 intendedPos;
	private Vector2 originalPos;

    [SerializeField]
	private bool local = false;
	
	void Awake () {
		originalPos = local ? transform.localPosition : transform.position;
		intendedPos = originalPos;
	}
	
	void Update () {
        if (local)
        {
            transform.localPosition = Vector2.Lerp((Vector2)transform.localPosition, intendedPos, Time.deltaTime * speed);
        }
        else
        {
            transform.position = Vector2.Lerp((Vector2)transform.position, intendedPos, Time.deltaTime * speed);
        }
	}
}

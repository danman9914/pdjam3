﻿using UnityEngine;
using System.Collections;

public class LerpAlpha : MonoBehaviour {

	public float speed = 2.5f;
	public float intendedAlpha;
	SpriteRenderer sR;
	TextMesh tM;
	
	void Awake () {
		sR = GetComponent<SpriteRenderer>();
		tM = GetComponent<TextMesh>();
	}
	
	void Update () {
		if (sR) {
			Color c = new Color(sR.color.r, sR.color.g, sR.color.b, Mathf.Lerp(sR.color.a, intendedAlpha, Time.deltaTime * speed));
			sR.color = c;
		}
		else if (tM) {
			Color c = new Color(tM.color.r, tM.color.g, tM.color.b, Mathf.Lerp(tM.color.a, intendedAlpha, Time.deltaTime * speed));
			tM.color = c;
		}
	}
}

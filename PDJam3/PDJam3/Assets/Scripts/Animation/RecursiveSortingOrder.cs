﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RecursiveSortingOrder : MonoBehaviour {

    [SerializeField]
    private int sortingOrder;

    [SerializeField]
    private bool sortOnStart = true;

    private int lastOrderChange;

    void Start()
    {
        if (sortOnStart)
        {
            SetSortingOrder(sortingOrder);
        }
    }

    public void SetSortingOrder()
    {
        SetSortingOrder(sortingOrder);
    }

    public void SetSortingOrder(int order)
    {
        foreach (SpriteRenderer s in GetComponentsInChildren<SpriteRenderer>())
        {
            s.sortingOrder += order - lastOrderChange;
        }

        foreach (ParticleSystem p in GetComponentsInChildren<ParticleSystem>())
        {
            p.GetComponent<Renderer>().sortingOrder += order - lastOrderChange;
        }

        lastOrderChange = order;
    }
}

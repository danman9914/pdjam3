﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LinearLerpPosition : MonoBehaviour {

	public bool Moving {
		get {
			return Vector2.Distance (transform.position, intendedPos) > 0.1;
		}
	}

	private Vector2 intendedPos;
	private float startTime;
	private float speed;

	void Awake()
	{
		intendedPos = transform.position;
	}

	void Update () {
		transform.position = Vector2.Lerp (transform.position, intendedPos, (Time.time - startTime) * speed);
	}

	public void LerpTo(Vector2 pos, float speed)
	{
		intendedPos = pos;
		startTime = Time.time;
		this.speed = speed;
	}
}

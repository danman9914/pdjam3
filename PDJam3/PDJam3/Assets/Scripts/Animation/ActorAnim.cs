﻿using UnityEngine;

public class ActorAnim : MonoBehaviour 
{
	private void ShowItem()
	{
		transform.FindChild ("rightarm/rightarm_lower/item").GetComponent<SpriteRenderer> ().enabled = true;
	}

    private void PlaySound(string trackName)
    {
    	AudioController.Instance.PlaySound(trackName);
    }

	private void PlayRandomSound(string trackName)
	{
		AudioController.Instance.PlayRandomSound(trackName, true);
	}

}

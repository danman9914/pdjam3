﻿using UnityEngine;
using System.Collections;

public class LerpScale : MonoBehaviour {

	public float speed = 2.5f;

	private Vector2 intendedScale;
    public Vector2 Scale
    {
        get
        {
            return intendedScale;
        }
        set
        {
            intendedScale = value;
        }
    }

	void Awake () {
		intendedScale = transform.localScale;
	}

	void Update () {
		transform.localScale = Vector2.Lerp((Vector2)transform.localScale, intendedScale, Time.deltaTime * speed);
	}
}

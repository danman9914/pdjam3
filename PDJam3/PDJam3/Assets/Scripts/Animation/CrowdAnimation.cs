﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CrowdAnimation : MonoBehaviour 
{

	public static CrowdAnimation instance;

	void Awake()
	{
		instance = this;
	}

	public void Cheer()
	{
		StartCoroutine (DoCheer ());
	}

	private void SetAnimSpeeds(float speed)
	{
		foreach (Animator anim in GetComponentsInChildren<Animator>())
		{
			anim.speed = speed;
		}
	}

	private IEnumerator DoCheer()
	{
		GetComponent<Animation> ().Play ();
		SetAnimSpeeds (3f);

		yield return new WaitForSeconds (1f);

		SetAnimSpeeds (1f);
	}

	#if UNITY_EDITOR
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.C))
		{
			Cheer ();
		}
	}
	#endif
}

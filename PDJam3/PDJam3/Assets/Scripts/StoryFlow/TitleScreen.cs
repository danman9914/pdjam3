﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour {

	[SerializeField]
	private LerpAlpha fadeOut;

	void Update () 
	{
		if (Input.GetMouseButtonDown(0) && Time.timeSinceLevelLoad > 0.5f)
		{
			StartCoroutine (ChangeScene ());
		}
	}

	IEnumerator ChangeScene()
	{
		AudioController.Instance.PlaySound("Applause");
		fadeOut.intendedAlpha = 1f;
		yield return new WaitForSeconds (4f);
		AudioController.Instance.SetLoopVolume (0.2f);
		SceneManager.LoadScene ("DIALOGUE_1");
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ActIntroController : MonoBehaviour {

	public static ActIntroController instance;

	[SerializeField]
	private string nextScene;

	[SerializeField]
	private LerpPosition pageFlipper;

	[SerializeField]
	private LerpPosition stageUI;

	[SerializeField]
	private LerpAlpha fade;

	void Awake () 
	{
		instance = this;
		StartCoroutine (DoIntro ());
	}

	IEnumerator DoIntro()
	{
		Vector2 originalUIPosition = stageUI.transform.localPosition;
		stageUI.LocalPosition = stageUI.LocalPosition + Vector2.up * 2.5f;

		fade.GetComponent<SpriteRenderer> ().color = new Color (0f, 0f, 0f, 1f);
		fade.intendedAlpha = 1f;
		yield return new WaitForSeconds (1f);
		fade.intendedAlpha = 0f;

		yield return new WaitForSeconds (1f);
		stageUI.LocalPosition = originalUIPosition;
	}

	public void EndScene()
	{
		StartCoroutine (DoOutro ());
	}

	IEnumerator DoOutro()
	{
		AudioController.Instance.FadeOutLoop ();
		fade.intendedAlpha = 1f;
		yield return new WaitForSeconds (2.5f);
		AudioController.Instance.CrossFadeLoop ("IntroPiece(roughMix)", 0.25f);
		yield return new WaitForSeconds (0.5f);
		SceneManager.LoadScene (nextScene);
	}
}

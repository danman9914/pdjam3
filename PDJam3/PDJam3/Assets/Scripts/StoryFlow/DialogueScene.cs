﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class DialogueScene : MonoBehaviour 
{
	[SerializeField]
	private SequentialText text;

	[SerializeField]
	private string nextScene;

	[SerializeField]
	private List<string> lines;

	void Start () {
		StartCoroutine (RunDialogue ());
		text.DisplayCharacter += OnCharacter;
	}

	void OnCharacter()
	{
		AudioController.Instance.PlaySoundWithPitch ("voice_13", 1f, 2f);
	}
	
	IEnumerator RunDialogue()
	{
		foreach (string line in lines)
		{
			text.PlayMessage (line, "");

			while (text.typing)
			{
				yield return new WaitForEndOfFrame ();
			}

			yield return new WaitForSeconds (0.5f);
		}
			
		if (nextScene != "DONE")
		{
			AudioController.Instance.FadeOutLoop ();
			text.Clear ();
			yield return new WaitForSeconds (2f);
			SceneManager.LoadScene (nextScene);
		}
	}
}
